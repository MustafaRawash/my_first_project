/*
This program takes a string from the user and calculates the number of vowels, consonants, digits, and white spaces.
*/

#include <stdio.h>
#include <conio.h>

void main()
{
    char strng[100], vwls[5]="aeiou", cnsnts[21]="bcdfghjklmnpqrstvwxyz";
    printf("Please enter a string with a maximum of 100 characters:\n");
    gets(strng);
    strlwr(strng);
    int i,j,vcount=0,ccount=0,dcount=0,wcount=0;
    for(i=0;i<strlen(strng);i++)
    {
        for(j=0;j<5;j++)
        {
            if(strng[i]==vwls[j])
            {
                vcount++;
                break;
            }
        }
        for(j=0;j<21;j++)
        {
            if(strng[i]==cnsnts[j])
            {
                ccount++;
                break;
            }
        }
        if((strng[i]>=48)&(strng[i]<=57))
        dcount++;
        if(strng[i]==' ')
        wcount++;
    }
    printf("Number of vowels = %d\nNumber of consonants = %d\nNumber of digits = %d\nNumber of white spaces = %d",vcount,ccount,dcount,wcount);
}
